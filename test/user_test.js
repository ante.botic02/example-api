"use strict"

const { getUserModel } = require("../data_access/modelFactory");
const assert = require("assert");

describe("Database", async() =>{

  it("Adds a user to the database", async () =>{
    try{
      const User = await getUserModel();

      const testUser = {
        firstName: "John",
        lastName: "Doe",
        username: "Melkor",
        password: "superStrongPWD",
        email: "Morgoth@thangodrim.beleriand"
      }

      const user = new User(testUser);

      user.save()
        .then(res => {
          assert.equal(res.firstName, "John");
        })
        .catch(err => console.log(err))
    } catch (err){
        throw err;
    }
  });

  it("Finds and removes a user from the database", async () =>{
    try {
      const User = await getUserModel();  
      
      await User.findOneAndRemove({firstName: "John", lastName: "Doe"});
      const result = await User.findOne({firstName:"John"});
      assert.equal(result, null);

    } catch(err) {
      throw err;
    }
  });

  it("Verifies that the password has been hashed", async() =>{
    try {
      const User = await getUserModel();

      const testUser = {
        password: "testpassword",
        email: "throwaway@mail.com"   
      };

      const user = new User(testUser);

      user.save()
        .then(res =>{
          const hashedPassword = res.password;
          asset.equal(hashedPassword, user.passwordIsValid(testUser.password));
        });

    } catch(err) {
      throw err;
    }

  });
});