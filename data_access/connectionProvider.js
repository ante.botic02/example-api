"use strict";

const mongoose = require("mongoose");

mongoose.Promise = global.Promise;
const _internalConnectionPool = {};

module.exports = function(url, database, options){

  return new Promise((resolve, reject) => {
    const adress = `mongodb://${url}/${database}`;
    const opts = Object.assign({}, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true
    }, options);

    if( !(_internalConnectionPool[adress])) {
      try {
        const conn = mongoose.createConnection(adress, opts);
        conn.on("open", function(){
          _internalConnectionPool[adress] = conn;
          resolve(_internalConnectionPool[adress]);
        });
        conn.on("error", (err)=> console.error("MongoDB error %s", err));
      } catch(err) {
        reject(err);
      }
    } else {
      return resolve( _internalConnectionPool[adress])
    }
  });
};