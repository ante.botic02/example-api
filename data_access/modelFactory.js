"use strict";

const connectionProvider = require("./connectionProvider");
const UserSchema = require("../schemas/User");
const keys = require("../config/keys");

const getUserModel = async function (){
  try {
    const conn = await connectionProvider(keys.mongoURI, keys.databaseName);
    return conn.model("User", UserSchema);

  } catch(err) {
    throw err;
  }
};

module.exports = {
  getUserModel
};