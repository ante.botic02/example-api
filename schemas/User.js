const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const Schema  = mongoose.Schema;

const UserSchema = new Schema({
  firstName: String,
  lastName: String,
  userName: {
    type: String,
    index:{
      unique: true
    }
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    required: true,
    default: new Date()
  }
});

UserSchema.pre("save", async function(next){
  if(!this.isModified("password")){
    return next();
  };

  try {
    const salt = await bcrypt.genSalt(14);
    const hash = await bcrypt.hash(this.password, salt);
    this.password = hash;
    next();

  } catch(err) {
      next(err);
  }
})

UserSchema.methods.passwordIsValid = async function(password) {
  try{
    return bcrypt.compare(password, this.password);
  } catch(err){
    throw err;
  } 
}


module.exports = UserSchema;
